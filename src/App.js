import React, { Component } from "react";

class App extends Component {
  state = {
    news: [],
    newsInput: "",
  };

  handleChange = e => {
    console.log("App handleChange", e.target.value);
    
    const value = e.target.value;
    this.setState(() => ({
      newsInput: value
    }));

  };

  handleKeyDown = e => {
    const { news, newsInput } = this.state;
    if (!newsInput) return;
    
    if (e.keyCode === 13) {
      this.setState(() => ({
        news: [...news, newsInput],
      }))
    }
  };

  render() {
    console.log('App');    
    
    const {news} = this.state;
    
    return (
      <div className="App" style={{backgroundColor: "DarkGray"}}>
        <input
          style={{margin: "20px 0 0 20px"}}
          onChange={this.handleChange}
          onKeyDown={this.handleKeyDown}
          type="text"
        />
        {news.map((newsContent, i) =>
          <NewsPost 
            key={i}
            text={newsContent}
          />
        )}
      </div>
    );
  }
}

class NewsPost extends Component {
  state = {
    comments:[],
    commentsInput: '',
  }

  handleChange = e => {
    const value = e.target.value;
    this.setState(() => ({
      commentsInput: value
    }));

  };

  handleKeyDown = e => {
    const {comments, commentsInput} = this.state;
    if (!commentsInput) return;

    if (e.keyCode === 13) {
      const comment = {
        id: Math.random().toString(36).substr(2, 9),
        text: commentsInput
      }
      
      this.setState(() => {
        return {
          comments: [...comments, comment],
        }
      })
    }
  };

  handleDelete = (id) => {
    console.log('delete!', id);
    let filteredState = this.state.comments.filter((comment) => comment.id !== id)
    console.log('filterstate', filteredState);
    
    this.setState({comments: filteredState})
    
  };

  componentDidUpdate() {
    console.log('thisatate', this.state);
    
  }

  render() {
    return (
      <div style={{backgroundColor: "grey", margin: "10px 0px 10px 50px", padding: "5px"}}>
        <p> {this.props.text}</p>  
        <input 
            onChange={this.handleChange} 
            onKeyDown={this.handleKeyDown}           
            type="text" 
          />
          {this.state.comments.map(({id, text}, i) =>
            <Comments key={i} id={id} text={text} onDelete={this.handleDelete} />
          )}
      </div> 
    )
  }
}

class Comments extends Component {
  handleChange = () => {};

  handleDelete = () => {};

  handleKeyDown = e => {};

  handleDelete = () => {
    this.props.onDelete(this.props.id);
  };

  render() {
    console.log("Comments");

    return (
      <div style={{ width: "50%", backgroundColor: "Beige", marginLeft: "150px" }}>
        <p> {this.props.text}</p>
        <span className="delete" style={{ color: "red" }} onClick={this.handleDelete} > X </span>
      </div>
    );
  }
}

export default App;